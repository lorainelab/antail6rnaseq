#!/bin/bash
# only works with samtools/0.1.19
RMATDIR=/lustre/groups/lorainelab/sw/rMATS.3.0.9
S=$RMATDIR/RNASeq-MATS.py
GTF=$RMATDIR/gtf/TAIR10.gtf
# prefix
P='w'
CONTR="${P}1.bam,${P}2.bam,${P}3.bam,${P}4.bam"
P='m'
TREAT="${P}1.bam,${P}2.bam,${P}3.bam,${P}4.bam"
DIR=AS
CMD="python $S -o $DIR -b1 $CONTR -b2 $TREAT -gtf $GTF -len 101 -t single"
echo "running: $CMD" > $DIR.out
$CMD 2>$DIR.err 1>>$DIR.out  
echo "DONE."
