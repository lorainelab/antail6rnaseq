#!/bin/sh

FILES=`ls *.bam`
JAR=/lustre/groups/lorainelab/bin/FindJunction_exe.jar
GENOME=A_thaliana_Jun_2009
TWOBIT=$QUICKLOAD/$GENOME/$GENOME.2bit
FLANK=5
echo $JAR
echo $TWOBIT
echo $FLANK
for F in $FILES
do
S=${F%.bam}
java -Xmx1g -jar $JAR -u -n $FLANK -b $TWOBIT -o $S.FJ.bed $F
sort -k1,1 -k2,2n $S.FJ.bed | bgzip > $S.FJ.bed.gz
rm $S.FJ.bed
tabix -s 1 -b 2 -e 3 -f -0 $S.FJ.bed.gz
done