#!/bin/bash
S=eco
G=A_thaliana_Jun_2009
B=TAIR10.bed
# SRR309153 - 11 day seedling, Col-0 1, C1
# SRR309154 - 11 day seedling, Col-0 2, C2
# SRR309163 - 11 day seedling, Ler 1, L1
# SRR309164 - 11 day seedling, Ler 2, L2
# Are these the same libraries? Confused.
# SRR314815 - SAMN00672502 SRS228279 Can_0_floral_bud_validation_rep1 F1
# SRR314818 - SRS228279 Can_0_floral_bud_validation_rep1 F2
PreParse.py -s _$S SRR309153.FJ.bed.gz SRR309154.FJ.bed.gz SRR309163.FJ.bed.gz SRR309164.FJ.bed.gz SRR314815.FJ.bed.gz SRR314818.FJ.bed.gz
ArabiTagMain.py -g $B -f 5 -s $S -e allJunctions_$S.bed
PostParse.py -b $B AltSplicing_$S.abt AS_$S.txt
Add_RiGp_Support.py -f 5 -d .. -i AS_$S.txt -o AS_${S}_wRI.txt
