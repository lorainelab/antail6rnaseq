Testing XlxsFuncs functions
========================================================

```{r include=FALSE}
funcsfile='XlxsFuncs.R'
```

This markdown file creates a short Excel file for testing functions defined in `r funcsfile`.

Load `r funcsfile`.

```{r include=FALSE}
source(funcsfile)
```

Read some data from differential expression analysis and take a small slice of it.

```{r}
fname='../DiffExpAnalysis/results/locusFC.tsv'
d=read.delim(fname,header=F)
names(d)=c('AGI','log2FC')
d=d[1:5,] 
```

Write an xlxs file to test that the functions in `r funcsfile` are working correctly. 

```{r}
fname='locusFC.xlsx'
wb=makeGeneListWorkbook(d,sheetName='log2FC',file=fname)
```