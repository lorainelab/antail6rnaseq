# RNA-Seq reveals roles for AINTEGUMENTA and AINTEGUMENTA-LIKE6 in cell wall remodeling and plant defense pathways in Arabidopsis

This repository contains data files and (mostly) R code used to analyze an RNA-Seq data set from flowers collected from wild-type and ant ail6 double mutant plants. Our goal was to identify targets (genes and
pathways) regulated by DNA binding proteins ANT and AIL6, which play important roles in plant growth and development. 

## About this project 

This project is a collaboration between the Krizek lab at University
of South Carolina and the Loraine Lab at the North Carolina Research
Campus in Kannapolis.

A grant from the US National Science Foundation to PI Krizek and Co-PI
Ann Loraine funded this work.

* * *

# About this repository

This repository contains analysis folders named for the type of data
analysis code and results they contain.

Most analysis folders contain:

* .Rproj file - an RStudio project file. Open this in RStudio to re-run project code.
* .Rmd files - R Markdown files that contain an analysis focused on answering one or two discrete questions. These are typically formatted like miniature research articles, with Introduction, Results/Analysis, and Conclusions sections. 
* .html files - output from running knitr on .Rmd files. 
* results folders - contain files (typically tab-delimited) and images produced by .Rmd files. 
* data - data files obtained from a compute cluster or other external source upstream of this data analysis
* src - R, python, or perl scripts used for simple data processing tasks
* README.md - documentation 

Code used for analysis, making figures, and making data files for
visualization in external site and programs are grouped into folders
with names indicating their common purpose or analysis type. 

Each module folder is designed to be run as a mostly
self-contained project in RStudio. As such, each folder contains an
".Rproj" file. To run the code in RStudio, just open the .Rproj file
and go from there. However, note that the code expects Unix-style file
paths and depends on external libraries, mainly from Bioconductor. 

Some modules depend on the output of other modules. Some modules also
depend on externally supplied data files, which are version-controlled
in ExternalDataSets but may also be available from external sites.

Unless otherwise noted, most code was written by both [Ann Loraine](http://lorainelab.org/people/loraine-bio/) and [Ivory Blakley](http://lorainelab.org/people/about-ivory) in the [Loraine Lab](http://www.lorainelab.org).

* * *

# What's here 

Analysis modules and other directories include .Rmd files (R
Markdown), output from knitting Rmd files in HTML (Web pages), folders
containing results (results) and (sometimes) folders containing
externally supplied data used only in one module (data). Results folders
also contain high-quality image files suitable for inclusion in slides or publications. 

* * *

## AIL6ReadsAnalysis

Used IGB to examine read alignments overlapping AIL6 in the
double mutant. 

Look here for SAM files containing read alignments for mutant
and wild-type samples. You can open these in IGB.

* * *

## AltSplicing 

Identifies alternatively spliced genes whose splicing patterns are
different between mutant and wild-type samples.

Look here for plots comparing percent-spliced in and other alternative
splicing statistics between sample types.

* * *

## AraCyc

This module investigates differential expression of genes encoding
metabolic pathway enzymes annotated in the AraCyc database.

Look here for files that can be uploaded into the AraCyc Omics viewer.

This module also contains code useful for converting AraCyc annotation
files to a format that can be read into GOSeq for pathway enrichment
analysis.

* * *

## CountsData

Makes counts, RPM, and RPKM files. Contains code for 

* processing counts data (Counts.Rmd)
* making RPM and RPKM files (MakeRpmRpkmFiles.Rmd)
* plotting expression levels (PlottingExpression.Rmd) 

Look in Counts/results for RPM, RPKM, and other counts-related results files.

* * *

## DiffExpAnalysis

Identifies differentially expressed genes using tools and libraries
from BioConductor.

Look here for results from differential expression analysis.

* * *

## ExternalDataSets

Contains gene annotations and other data sets downloaded from IGBQuickLoad.org, GeneOntology.org, and other sources.

* * *

## GOSeq

Identifies over and under represented GO categories among differentially expressed genes using the GOSeq library from BioConductor.

* * *

## GOrillaAgriGO

Same as GOSeq, but used on-line tools GOrilla and AgriGO.

* * *

IgbPics

Contains genome browser images made using Integrated Genome Browser.

* * * 

# Questions?

Contact:

* Ann Loraine aloraine@uncc.edu
* Beth Krizek krizek@sc.edu

* * *

# License 

Copyright (c) University of North Carolina at Charlotte

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Also, see: http://opensource.org/licenses/MIT