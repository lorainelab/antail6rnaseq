#!/bin/bash

D=${1:-"."}
R='chr5:3,315,207-3,320,250'
W='Ler1 Ler2 Ler3 ant5'
M='ant1 anr2 ant3 Ler5'

SM='W.sm'
MM='W.mm'

# make header
samtools view -H $D/Ler1.sm.bam > $SM.sam
samtools view -H $D/Ler1.sm.bam > $MM.sam

# get reads for region
for S in $W
do
    samtools view $D/$S.sm.bam $R >> $SM.sam
    samtools view $D/$S.mm.bam $R >> $MM.sam
done

SM='M.sm'
MM='M.mm'

# make header
samtools view -H $D/ant1.sm.bam > $SM.sam
samtools view -H $D/ant1.sm.bam > $MM.sam

# get reads for region
for S in $M
do
    samtools view $D/$S.sm.bam $R >> $SM.sam
    samtools view $D/$S.mm.bam $R >> $MM.sam
done

