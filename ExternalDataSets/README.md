# Data files from external sources

* * *

## ArabidopsisTranscriptionFactors.xls

text

* * *

## aracyc_pathways.20130709.gz 

text

* * *

## gene_association.tair.gz

text

* * *

## gene_info.txt.gz

text

* * *

## gene_ontology_ext.obo.gz

text

* * *

## genedescr.tsv.gz

text

* * *

## Giraut-2011-TableS2.XLS

Supplemental table S2 taken directly from ["Genome-Wide Crossover Distribution in Arabidopsis thaliana Meiosis Reveals Sex-Specific Patterns along Chromosomes"](http://journals.plos.org/plosgenetics/article?id=10.1371/journal.pgen.1002354#s4) by Giraut et al.

This file describes the regions that they considered for cross overs and the cross over reates they found in each for male and femial meiosis.

* * *

## LerDiffs.bed

Bed file describing differences between the Columnbia genome and the Landsberge erecta genome. This file was generated in separate project based on the variations file from [19 Genomes Project](http://mus.well.ox.ac.uk/19genomes/).

* * *

## TAIR10.bed.gz

text

* * *

## tx_size.txt.gz

text

